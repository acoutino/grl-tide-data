close all;clear all;
cd SianKaanDec2019
load siankaan_depth
date_sian = date;
depth_sian = depth;
cd ../ChukumDec2019
load chukum_depth
date_chukum = date;
depth_chukum = depth;

start_loc = findclosestindex(date,datetime(019,07,26));
end_loc = findclosestindex(date,datetime(019,12,2));

date = date(start_loc:end_loc);
depth_chukum = depth_chukum(start_loc:end_loc);
depth_sian = depth_sian(start_loc:end_loc);

plot(date,depth_chukum,date,depth_sian)

figure
wcoherence(depth_sian,depth_chukum,hours(0.5))