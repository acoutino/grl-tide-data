close all;clear all;
cd SianKaanDec2019
load siankaan_depth
date_sian = date;
depth_sian = depth;
cd ../ChichDec2019
load chich_depth
date_chich = date;
depth_chich = depth;

start_loc = findclosestindex(date,datetime(019,07,26));
end_loc = findclosestindex(date,datetime(019,12,2));

date = date(start_loc:end_loc);
depth_chich = depth_chich(start_loc:end_loc);
depth_sian = depth_sian(start_loc:end_loc);

plot(date,depth_chich,date,depth_sian)

figure
wcoherence(depth_sian,depth_chich,hours(0.5))