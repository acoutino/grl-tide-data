

function [ar2tab] = AR2(nt,rho1,rho2,nsim)
rzero=0;
ar2tab=[];
sig=[];
srho=sqrt(1-rho1^2-rho2^2);
for i=1:nsim
    white=srho*randn(1);
    sig(1)=white;
    sig(2)=rho1*sig(1)+white;
    for j=3:nt
        white=srho*randn(1);
        sig(j)=rho1*sig(j-1)+rho2*sig(j-2)+white;
    end
    ar2tab(:,i)=sig;
end
end

