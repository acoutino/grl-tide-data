function sse = sseval(var,f,depthspec)
sse = sum((depthspec - (4*var/length(f))./(1+0.91403^2+0.085544^2-2*0.91403*(1-0.085544)*cos(2*pi*f)-2*0.085544*cos(4*pi*f))).^2);