%%
close all;clear all;
load('chich_depth.mat')
dt = 0.5;
nsims = 100;

[Mspecred,specred,pd,fd,fr,rho] = RedConf(depth,datenum(date),dt,nsims,[]);

plot(fd,10*log10(pd))
hold
plot(fr,10*log10(Mspecred))