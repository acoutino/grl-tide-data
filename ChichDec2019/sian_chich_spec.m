close all;clear all;
load chich_depth
[pxx,f,pxxc] = periodogram(depth,rectwin(length(depth)),length(depth),2,'ConfidenceLevel',0.95);
plot(f,10*log10(pxx))
hold
cd ../SianKaanDec2019/
load siankaan_depth.mat
[pxx,f,pxxc] = periodogram(depth,rectwin(length(depth)),length(depth),2,'ConfidenceLevel',0.95);
plot(f,10*log10(pxx))
grid on
xlabel('Frequency (per hour)')
ylabel('Spectral Power (dB)')
axis([0 0.5 -80 0])
legend('Chichankanab','Sian Kaan')
cd ..
