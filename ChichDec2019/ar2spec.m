close all;clear all;
load chich_depth
[pxx,f,pxxc] = periodogram(depth,rectwin(length(depth)),length(depth),2,'ConfidenceLevel',0.95);
plot(f,10*log10(pxx))
hold
txt = '\leftarrow 24.0327 hrs';
txt2 = '\leftarrow 12.0149 hrs';
txt3 = '\leftarrow 7.9872 hrs';
txt4 = '\leftarrow 3.9952 hrs';
text(0.04161,-10.15,txt,'FontWeight','bold')
text(0.08323,-18.17,txt2,'FontWeight','bold')
text(0.12520,-29.79,txt3,'FontWeight','bold')
text(0.25030,-34.14,txt4,'FontWeight','bold')
var=0.4641;
n=6248;
rho1=0.91403;
rho2=0.0855441;
chi2lvl = 0.95;
f=linspace(0,1,6248);
sf = ar2(var,n,f,rho1,rho2);
plot(f,10*log10(sf))
chi2_qua = chi2inv(chi2lvl,2);
conf_int = chi2_qua/2*sf;
plot(f,10*log10(conf_int),'--')

legend('Depth Power Spectra','Null Spectra','95% Confidence Level')
xlabel('Frequency (per hour)')
ylabel('Spectral Power (dB)')
grid on
axis([0 0.35 -60 10])
