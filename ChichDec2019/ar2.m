function Sf = ar2(var,n,f,rho1,rho2)
num = 4*var/n;
den = 1+rho1^2+rho2^2-2*rho1*(1-rho2)*cos(2*pi*f) - 2*rho2*cos(4*pi*f);
Sf = num./den;
