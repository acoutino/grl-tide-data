close all;clear all;
load('chich_depth.mat')

[pxx,f,pxxc] = periodogram(depth,rectwin(length(depth)),length(depth),2,'ConfidenceLevel',0.95);
plot(f,10*log10(pxx))
hold on
plot(f,10*log10(pxxc),'-.')
txt = '\leftarrow 24.0327 hrs';
txt2 = '\leftarrow 12.0149 hrs';
txt3 = '\leftarrow 7.9872 hrs';
txt4 = '\leftarrow 3.9952 hrs';
text(0.04161,-10.15,txt)
text(0.08323,-18.17,txt2)
text(0.12520,-29.79,txt3)
text(0.25030,-34.14,txt4)
axis([0 0.35 -80 40])
grid on
xlabel('Frequency (per hour)')
ylabel('Spectral Power (dB)')
publishpic