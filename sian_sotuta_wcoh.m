close all;clear all;
cd SianKaanDec2019
load siankaan_depth
date_sian = date;
depth_sian = depth;
cd ../SotutaDec19
load sotuta_depth
date_sotuta = date;
depth_sotuta = depth;

start_loc = findclosestindex(date,datetime(019,07,26));
end_loc = findclosestindex(date,datetime(019,12,2));

date = date(start_loc:end_loc);
depth_sotuta = depth_sotuta(start_loc:end_loc);
depth_sian = depth_sian(start_loc:end_loc);

plot(date,depth_sotuta,date,depth_sian)

figure
wcoherence(depth_sian,depth_sotuta,hours(0.5))