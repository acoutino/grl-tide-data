close all;clear all;
cd SianKaanDec2019\
load siankaan_depth.mat
bandpass(depth,[1/48 1/5])
subplot(2,1,1)
ylim([-0.02 0.02])
cd ../ChichDec2019
load chich_depth.mat
figure
bandpass(depth,[1/48 1/5])
subplot(2,1,1)
ylim([-0.02 0.02])
cd ../SotutaDec19/
load sotuta_depth.mat
figure
bandpass(depth,[1/48 1/5])
subplot(2,1,1)
ylim([-0.02 0.02])
cd ../ChukumDec2019/
load chukum_depth.mat
figure
bandpass(depth,[1/48 1/5])
subplot(2,1,1)
ylim([-0.02 0.02])
cd ../PacChenDec2019/
load pacchen_deptht.mat
figure
bandpass(depth,[1/48 1/5])
subplot(2,1,1)
ylim([-0.02 0.02])
cd ..
