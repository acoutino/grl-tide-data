close all;clear all;
cd SianKaanDec2019
load siankaan_depth.mat
figure
plot(date,depth)
xlim([date(1) date(end)])
xlabel('Date')
ylabel('Water Level (m)')
grid on
publishpic
cd ..
print -dpdf Sian_full.pdf

cd ChichDec2019
load chich_depth.mat
figure
plot(date,depth)
xlim([date(1) date(end)])
xlabel('Date')
ylabel('Water Level (m)')
grid on
publishpic
cd ..
print -dpdf Chich_full.pdf

cd SotutaDec19
load sotuta_depth.mat
figure
plot(date,depth)
xlim([date(1) date(end)])
xlabel('Date')
ylabel('Water Level (m)')
grid on
publishpic
cd ..
print -dpdf Sotuta_full.pdf

cd ChukumDec2019
load chukum_depth.mat
figure
plot(date,depth)
xlim([date(1) date(end)])
xlabel('Date')
ylabel('Water Level (m)')
grid on
publishpic
cd ..
print -dpdf Chukum_full.pdf

cd PacChenDec2019
figure
plot(date,depth)
xlim([date(1) date(end)])
xlabel('Date')
ylabel('Water Level (m)')
grid on
publishpic
cd ..
print -dpdf Pac_full.pdf
