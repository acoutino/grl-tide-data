close all;clear all;
cd SianKaanDec2019
load siankaan_depth
date_sian = date;
depth_sian = depth;
cd ../PacChenDec2019
load pacchen_deptht
date_pac = date;
depth_pac = depth;

start_loc = findclosestindex(date,datetime(019,07,26));
end_loc = findclosestindex(date,datetime(019,12,2));

date = date(start_loc:end_loc);
depth_pac = depth_pac(start_loc:end_loc);
depth_sian = depth_sian(start_loc:end_loc);

plot(date,depth_pac,date,depth_sian)

figure
wcoherence(depth_sian,depth_pac,hours(0.5))